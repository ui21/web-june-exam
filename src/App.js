import "bootstrap/dist/css/bootstrap.css";
import NavBar from './components/NavBar'
import './app.css'
import React from 'react'
import { useSelector, useDispatch } from "react-redux";
import {Container} from 'react-bootstrap'
import {withDraw, depositMoney} from './state/action-creators'
import { bindActionCreators } from "redux";

export default function App() {
  const state = useSelector((state) => state)
  const dispatch = useDispatch();
  const deposit_money = bindActionCreators(depositMoney, dispatch);
  const withdraw_money = bindActionCreators(withDraw, dispatch);
  return (
    <div>
      <NavBar/>
      <Container>
        <h1>{state.account}</h1>
        <button onClick={()=> deposit_money(1000)}> + </button>
        <button onClick={()=> withdraw_money(1000)}> - </button>
      </Container>
    </div>
  )
}
